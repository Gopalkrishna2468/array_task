function filter(elements, cb) {
  let filtered =[];
  for ( let value of elements ){
    cb(value) ? filtered.push(value) : null;
  }
  return filtered;
}

module.exports = filter;