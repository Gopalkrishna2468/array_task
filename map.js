function map(elements, cb) {
    let returnArray = [];
  for ( let value of elements ){
    returnArray.push(cb(value));
  }
  return returnArray;
}

module.exports=map;