

function reduce(elements, cb, startingValue) {

  let index = startingValue || 0;
  const control = elements[index];
  let reduced = control;
  for ( let i = index+1; i < elements.length; ++i ) {
    reduced = cb(reduced, elements[i] )
  }
  return reduced;
}
module.exports=reduce;