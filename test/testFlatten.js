const items = require("../items");
const flatten = require("../flatten.js");

const nestedArray = [1, [2], [[3]], [[[4]]]];

const result = flatten(items, cb);
